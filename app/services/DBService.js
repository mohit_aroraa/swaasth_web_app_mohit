(function() {

    'use strict';

    var DBService = function($q,Loki) {
        var dbService = {};

        var _currentUser,_db;
        /*var _misc;
        var fsAdapter = new LokiCordovaFSAdapter({"prefix": "loki"});
        var _db = new Loki('swaasthDB',
            {
                autosave:false,
                autload:true,
                adapter: fsAdapter
            });*/

        dbService.initDB = function () {
            console.log("InitDB called");
            var fsAdapter = new LokiCordovaFSAdapter({"prefix": "loki"});
            _db = new Loki('swaasthDB',
               {
                   autosave:false,
                   adapter: fsAdapter
               });
        };

        dbService.addCurrentUser = function(user, callback){
            var options = {};
            _db.loadDatabase(options, function () {

                _currentUser = _db.getCollection('currentUser');
                
                if (!_currentUser) {
                    _currentUser = _db.addCollection('currentUser');

                }
                _currentUser.insert(user);

                _db.saveDatabase();
                console.log("successfully inserted",_db.getCollection('currentUser'));

                if(callback){
                    callback();
                }
            });

        };

        dbService.updateCurrentUser = function(user){
            
            console.log("before updating",_db.getCollection('currentUser').data[0]);
            
            _currentUser.update(user);
            _db.saveDatabase();
            
            console.log("successfully updated",_db.getCollection('currentUser').data[0]);

        };


        dbService.AddCurrentUserDoc = function(){

            return $q(function (resolve, reject) {
                var options = {};

                _db.loadDatabase(options, function () {
                    _currentUser = _db.getCollection('currentUser');

                    if (!_currentUser) {
                        _currentUser = _db.addCollection('currentUser');
                    }

                    resolve(_currentUser.data);
                });

            });

        };

        dbService.getCurrentUser = function(){

            return $q(function (resolve, reject) {
                var options = {};
                _db.loadDatabase(options, function () {
                    _currentUser = _db.getCollection('currentUser');
                    
                    if (!_currentUser) {
                        resolve({});
                    } else {
                        resolve(_currentUser.data[0]);
                    }
                    
                });

            });

        };

        dbService.removeUser = function(callback){
            var options = {};
            _db.loadDatabase(options, function () {
                
                _currentUser = _db.getCollection('currentUser');
                if(_currentUser){    
                    _currentUser = _db.removeCollection('currentUser');
                }

                _db.saveDatabase();
                console.log("user removed",_db.getCollection('currentUser'));
                if(callback){
                    callback();
                }
            });

        };

        return dbService;

    };

    DBService.$inject = [
        '$q',
        'Loki'
    ];

        angular
            .module('swaasth')
            .factory('DBService', DBService);
    })();