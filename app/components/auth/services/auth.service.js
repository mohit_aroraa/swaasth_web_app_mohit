/**
 * @description 
 * This basically authenticates and registers the user and 
 * performs other operations like logging out the user.
 *              
 * @author Tushar
 */
(function() {

    'use strict';

    var AuthService = function($q, $http, SessionService, $localStorage, BASE_URL, API) {

        var authService = {};
        
        /**
         * @description
         * Allows user to login to the application and saves 
         * the auth-token and userDetails in localStorage.
         *
         * @param  {object} credentials [Contains emailId or mobile and password]
         * 
         * @author
         * Tushar
         */


        authService.login = function(credentials) {
            // console.log("url="+BASE_URL.url+API.login+"data="+credentials);

            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.login, { userName: credentials.userName, password: credentials.password })
                .then(function(res) {
                    // console.info('AuthService [Login] : : ', res);
                    if (res.data.statusCode === 200) {
                        var token = res.data.userDetails.authToken;
                        delete res.data.userDetails.authToken;
                        var currentUser = res.data.userDetails;
                        //Lokiwork.setCurrentDoc('settings', 'globals', {'name': "user settings"}).then(function(){
                        //
                        //    Lokiwork.updateCurrentDoc("isFirstLogin", res.data.isFirstLogin);
                        //    Lokiwork.updateCurrentDoc("authToken", token);
                        //    var currentSettings = Lokiwork.getCurrentDoc();
                        //    console.log("currentSettings=",currentSettings);
                        //
                        //
                        //});

                        // Lokiwork.updateDoc("settings", "globals", {name:"user settings"}, "isFirstLogin", false);
                        // Lokiwork.updateDoc("settings", "globals", {name:"user settings"}, "authToken", token);

                        var db = null;
                        var currentUserDoc;

                        if(res.data.isFirstLogin) {

                            //DBService.createCurrentUserDoc();
                            //currentUserDoc = db.addCollection('currentUser');
                        }
                        
                        if (!isDesktop) {

                            DBService.removeUser(function(){
                                DBService.addCurrentUser(currentUser,function(){
                                    deferred.resolve(res.data);
                                });
                            });
                        } else {
                            //localStorage.setItem('ngStorage-authToken', token);
                            localStorage.setItem('currentUser', JSON.stringify(currentUser));
                            deferred.resolve(res.data);
                        }

                        //DBService.getOrAddCurrentUserDoc().then(function(currentUser){
                        //
                        //    console.log("DBlength",currentUser);
                        //
                        //});
                        //
                        //DBService.addMisc({isFirstLogin:res.data.isFirstLogin,screenNumber:0,authToken:token});

                        /*if(db){

                            currentUserDoc = db.getCollection('currentUser');
                           //var newUser =  db.findOne({'id':currentUser.id});
                           currentUserDoc.insert(res.data.userDetails);
                           console.log("DBlength",currentUserDoc);

                          // logObject(db.getCollection('currentUser'),true);
                        }
*/
                        $localStorage.$default({
                            isFirstLogin: res.data.userDetails.isFirstLogin,
                            screenNumber: res.data.screenNo
                        });

                        var user = res.data.userDetails;
                        SessionService.create(token, user);
                        //deferred.resolve(res.data);
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });


            return deferred.promise;

        };



        authService.logout = function() {
            var deferred = $q.defer();
            $http.post('/logout')
                .then(function(res) {
                    console.info('Logging Out..');
                    SessionService.destroy();
                    deferred.resolve(res);
                }).catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;

        };

        authService.checkPassword = function(present) {

            var deferred = $q.defer();

            $http
                .post(BASE_URL.url + API.checkPassword, present )
                .then(function(res) {
                    console.info('AuthService [checkPassword] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        /**
         * @description
         * Fetches the current user from localStorage.
         *
         * @return
         * {object} Current User
         *
         * @author
         * Tushar 
         */
        authService.getCurrentUser = function() {
            console.info('authService [getCurrentUser] Called');
            return $q(function(resolve,reject){

                if (isDesktop) {
                    
                    var currentUser = localStorage.getItem('currentUser');
                    resolve(JSON.parse(currentUser));

                } else {

                    DBService.getCurrentUser().then(function(currentUser){
                        resolve(currentUser);
                    });
                }

            });
            //return $localStorage.currentUser;
        };

        /**
         * @description Checks whether the user is authenticated or not.
         * 
         * @author 
         * Tushar
         */
        authService.isAuthenticated = function() {
            console.info('authService [isAuthenticated] Called');
            //return !!$localStorage.authToken;

            /*var deferred = $q.defer();
            var token = $localStorage.authToken;
            $http
                .post(BASE_URL.url + API.checkAuthtoken, { authToken: token})
                .then(function(res) {
                    console.info('AuthService [resetPassword] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);
                        
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;*/
        };


        /**
         * @description
         * Makes an API request to `/forgotPassword`, and returns the OTP
         *
         * @param  {string} email [User provided email Id]
         * 
         * @return {object} [which must contain OTP]
         *
         * @author
         * Tushar
         */
        authService.forgotPassword = function(email) {

            var deferred = $q.defer();

            $http
                .post(BASE_URL.url + API.forgotPassword, { emailId: email })
                .then(function(res) {
                    console.info('AuthService [forgotPassword] : : ', res);
                    if (res.data.statusCode === 200) {
                        $localStorage.$default({
                            email: email
                        });
                        deferred.resolve(res.data);
                    } else {
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        authService.sendAccessToken = function(params){
            var deferred = $q.defer();            
            $http
                .post(BASE_URL.url + API.socialLogin, { access_token: params.access_token, type: params.type })
                .then(function(res) {                    
                    if (res.data.statusCode === 200) {
                        console.info('AuthService [sendAccessToken] Success ::', res);
                        var token = res.data.userDetails.authToken;
                        delete res.data.userDetails.authToken;
                        var currentUser = res.data.userDetails;

                        var db = null;
                        var currentUserDoc;

                        if(res.data.isFirstLogin) {

                            //DBService.createCurrentUserDoc();
                            //currentUserDoc = db.addCollection('currentUser');
                        }
                        
                        if (isDesktop) {
                            localStorage.setItem('ngStorage-authToken', token);
                            localStorage.setItem('currentUser', JSON.stringify(currentUser));
                            deferred.resolve(res.data);

                        } else {

                            DBService.removeUser(function(){
                                DBService.addCurrentUser(currentUser,function(){
                                    deferred.resolve(res.data);
                                });
                            });
                        }

                        /*$localStorage.$default({
                            isFirstLogin: res.data.userDetails.isFirstLogin,
                            screenNumber: res.data.screenNo
                        });*/

                        var user = res.data.userDetails;
                        SessionService.create(token, user);

                        //deferred.resolve(res.data);
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        /**
         * @description
         * Makes an API request to `/verifyOTP`.
         * Validates the user provided OTP and sends the 
         * response accordingly.
         *
         * @param  {number} otp [Contains six digit otp]
         * 
         * @author
         * Tushar
         */
        authService.validateOTP = function(otp) {

            var deferred = $q.defer();
            var email = $localStorage.email;
            $http
                .post(BASE_URL.url + API.verifyOTP, { emailId: email, otp: otp })
                .then(function(res) {
                    console.info('AuthService [validateOTP] : : ', res);
                    if (res.data.statusCode === 200) {
                        if ($localStorage.authToken) {
                            $localStorage.authToken = res.data.authToken;
                        } else {
                            $localStorage.$default({
                                authToken: res.data.authToken
                            });
                        }
                        deferred.resolve(res.data);
                    } else {
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };



        /**
         * @description
         * Makes an API request to `/resetPassword`.
         * Resets password with the user provided password.
         *
         * @param  {string} token [ User provided password ]
         * 
         * @author
         * Tushar
         */
        authService.resetPassword = function(password, userObj) {

            var deferred = $q.defer();
            var fromSignUp = userObj.fromSignUp || false;
            var token = $localStorage.authToken;
            
            if ( fromSignUp ){
                var url = BASE_URL.url + API.signup;
                var params = { email: userObj.emailId, 
                    contactNumber: userObj.contactNumber, 
                    password: password 
                }                
            }else{
                var url = BASE_URL.url + API.resetPassword;
                var params = { authToken: token,
                    password: password 
                }
            }
            $http
                .post(url, params)
                .then(function(res) {
                    console.info('AuthService [resetPassword] : : ', res);
                    if (res.data.statusCode === 200) {                        
                        if ( fromSignUp ){
                            $localStorage.isFirstLogin = 'yes';
                        }else{
                            $localStorage.isFirstLogin = 'no';
                        } 
                        deferred.resolve(res.data);
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        authService.saveDeviceToken = function(authToken){            
            var push = PushNotification.init(
                { 
                    "android": {"senderID": "936106748065"},
                    "ios": {"alert": "true", "badge": "true", "sound": "true"},
                    "windows": {} 
                } 
            );

            push.on('registration', function(data) {
                console.log(data.registrationId);
                var params = {
                    authToken: authToken,
                    deviceToken: data.registrationId
                }
                $http
                .post(BASE_URL.url + API.addDeviceToken, params)
                .then(function(res) {
                    console.info('AuthService [getGCMRegisterationID] : : ', res);
                    if (res.data.statusCode === 200) {                        
                        console.log("Device Token successfully saved");
                    } else {
                        console.log("Error While saving GCM deviceToken, status code is :", res.data.statusCode);
                    }
                }).catch(function(error) {
                    console.log("Error While saving GCM deviceToken", error);
                });
                                
            });
            push.on('error', function(e) {
               console.log("Error While saving GCM deviceToken", e);
            });
            push.on('notification', function(data) {
                console.log(data.message);

                var home = $('.home-icon').parent();
                var html = '<div class="rec-batch" style="position:absolute;width: 20px;height: 20px;background-color: red;border-radius: 15px;right: 28px;top: 2px;">'+parseInt(data.count)+'</div>';

                if (location.hash != "#/home/recommendationsList") {
                    home.append(html);
                }

                //alert(data.title+" Message: " +data.message);
                 //data.title,
                 //data.count,
                 //data.sound,
                // data.image,
                // data.additionalData
            });
            
        }

        authService.checkUserExists = function(emailId) {

            var deferred = $q.defer();
            var url = BASE_URL.url + API.checkUserExists;
            var params = {emailId: emailId};
            $http
                .post(url, params)
                .then(function(res) {
                    console.info('AuthService [resetPassword] : : ', res);
                    if (res.data.statusCode === 200) {                        
                        deferred.resolve(res.data);
                    } else {
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

                return deferred.promise;
        }
        return authService;
    };

    AuthService.$inject = [
        '$q',
        '$http',
        'SessionService',
        '$localStorage',
        'BASE_URL',
        'API'
    ];


    angular
        .module('swaasth.auth')
        .factory('AuthService', AuthService);
})();
