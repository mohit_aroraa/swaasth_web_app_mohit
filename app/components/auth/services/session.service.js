/**
 * @description 
 * Creates the fresh new session for the user 
 * who has logged in to the application
 *              
 * @author Tushar
 */
(function() {

    'use strict';

    var SessionService = function($q, $localStorage) {
        var oldDate = new Date();
        var expiryDate = new Date(oldDate.getTime() + 60 * 1000);
        var session = {};
        session.create = function(token, user) {
            console.info('SessionService [create] Called', token);

            //$localStorage.currentUser = user;
            //DBService.addCurrentUser(user);

            $localStorage.authToken = token;
            //localStorage.setItem('ngStorage-authToken', token);

        };


        /**
         * @description Removes the session when user logged out.    
         * @author Tushar
         */
        session.destroy = function() {
            console.info('SessionService [destroy] Called');
            // sessionStorage.removeItem('authToken');
            localStorage.clear();
            $localStorage.$reset();
        };

        return session;
    };

    SessionService.$inject = ['$q', '$localStorage'];


    angular
        .module('swaasth.auth')
        .factory('SessionService', SessionService);
})();
