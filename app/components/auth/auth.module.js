var auth = angular.module("swaasth.auth", []);

auth.config(['$stateProvider', '$urlRouterProvider', 'AuthRoutes', function($stateProvider, $urlRouterProvider, AuthRoutes) {
    $stateProvider
        .state(AuthRoutes.login.name, {

            url: AuthRoutes.login.url,
            templateUrl: 'components/auth/partials/login.html',
            // animation: 'slide-left',
            /*resolve: {
                auth: ['AuthService', '$q', function(AuthService, $q) {
                    var deferred = $q.defer();
                    if (AuthService.isAuthenticated()) {
                        deferred.reject('authenticated');
                    } else {
                        deferred.resolve('unauthenticated');
                    }
                    return deferred.promise;
                }]
            }*/


        }).state(AuthRoutes.forgotPassword.name, {
            url: AuthRoutes.forgotPassword.url,
            templateUrl: 'components/auth/partials/forgot_pwd.html'

        }).state(AuthRoutes.signUp.name, {
            url: AuthRoutes.signUp.url,
            templateUrl: 'components/auth/partials/signUp.html'

        }).state(AuthRoutes.setPassword.name, {
            url: AuthRoutes.signUp.url,
            templateUrl: 'components/auth/partials/reset_pwd.html'

        })
        .state(AuthRoutes.otp.name, {
            url: AuthRoutes.otp.url,
            templateUrl: 'components/auth/partials/otp.html'

        }).state(AuthRoutes.resetPassword.name, {
            url: AuthRoutes.resetPassword.url,
            templateUrl: 'components/auth/partials/reset_pwd.html'

        }).state(AuthRoutes.changePassword.name, {
            url: AuthRoutes.changePassword.url,
            templateUrl: 'components/auth/partials/changePassword.html'

        });
}]);
