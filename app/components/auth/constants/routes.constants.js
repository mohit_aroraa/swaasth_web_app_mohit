/*globals angular */
(function() {
    'use strict';

    /**
     * @description Contains the Routes Constants, which are available across the swaasth.auth module.
     * @author Tushar
     */
    angular.module('swaasth.auth').constant('AuthRoutes', {

        /**********************************************************************
                    AUTH ROUTES
        **********************************************************************/

        login: {
            name: 'login',
            url: '/login'
        },

        forgotPassword: {
            name: 'forgotPassword',
            url: '/forgotPassword'
        },

        signUp: {
            name: 'signUp',
            url: '/signUp'
        },
        
        setPassword: {
            name: 'setPassword',
            url: '/setPassword'
        },

        resetPassword: {
            name: 'resetPassword',
            url: '/resetPassword'
        },

        otp: {
            name: 'otp',
            url: '/otp'
        },

        dashboard: {
            name: 'dashboard',
            url: '/dashboard'
        },

        changePassword: {
            name: 'changePassword',
            url: '/changePassword'
        }
    });
})();
