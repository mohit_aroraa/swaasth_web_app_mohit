/**
 * @description
 * Manages the login of the user and allows user to use the application
 * on successful login
 *
 * @author
 * Tushar
 */
(function() {

    'use strict';

    angular.module('swaasth.auth')
        .controller('AuthController', ['$rootScope', '$scope', '$state', 'AuthService', 'AuthRoutes','$q', '$http', 'SessionService',
            function($rootScope, $scope, $state, AuthService, AuthRoutes,$q, $http, SessionService) {

            $scope.login = {
                userName: '',
                password: ''
            };
            $scope.login.uNamecls = false;
            $scope.login.pwdcls = false;
            $scope.changePwdFlag = false;

            ///editttttttttttt
            /*if($state.current.name == 'login'){
                $('#indexCloud').addClass('cloud-down');
                $('#indexCloud').removeClass('cloud-light');
            } else {
                $('#indexCloud').removeClass('cloud-down');
                $('#indexCloud').addClass('cloud-light');
            }*/
            
            /**
             * @description
             * Receive broadcast event on device ready passed from application.js->application controller
             *
             * @author
             * Abrajeethan
             */

            $scope.$on('DeviceReady',function(event){
                console.log("isDeviceReady::broadcast called");
                //$cordovaProgress.showSimple();
            });

            /**
            * @description
            * Signup using emailid
            *
            * @author
            * Srikanth N S
            */            
            $scope.checkValidUser = function(){
                console.log("I am here", $rootScope.signUpObj.emailId); 
                console.log("I am here", $rootScope.signUpObj.contactNumber);
                $scope.errorMessage = "";
                $rootScope.showLoader();
                AuthService.checkUserExists($rootScope.signUpObj.emailId).then(function(response) {
                    console.info('forgotPassword : : ', response);
                    $rootScope.hideLoader();
                    if (response.statusCode == 200){                        
                        $rootScope.signUpObj.fromSignUp = true; 
                        console.log("Is from signUp? ::", $rootScope.signUpObj.fromSignUp);
                        $state.go(AuthRoutes.setPassword.name);
                    } else {
                        //$rootScope.signUpObj.emailId = "";
                        $scope.errorMessage = "Email ID Already Exists";
                    }
                }).catch(function(error) {
                    $rootScope.hideLoader();
                    console.log('Error in forgot password : : ', error);
                    $rootScope.signUpObj.emailId = "";
                    $rootScope.signUpObj.contactNumber = "";
                    //$scope.errorMessage = "Email ID Already Exists";
                    $scope.hasError = true;
                });



                
            };
            

            /**
             * @description
             * Allows user to signin to the application
             *
             * @author
             * Tushar
             */
            $scope.hasError = false;
            $scope.signin = function(loginForm) {
                //loginForm.$submitted=true;
                // ActivityIndicator.show();

                if (loginForm.$valid) {
                    // ActivityIndicator.show();
                    $rootScope.showLoader();
                    AuthService.login($scope.login).then(function success(response) {
                        console.info('success Response : : ', response);

                        var screenNumber = response.screenNo;
                        var userDetails = response.userDetails;

                        if (userDetails.isFirstLogin === "yes") {
                            
                            $state.go(AuthRoutes.resetPassword.name);
                        } else {
                            //$state.go($scope.profileScreen[screenNumber]);
                            //$state.go('profile.intro');
                            /*which is present in application controller*/
                            //$scope.setCurrentUser(userDetails);
                            setTimeout(function(){
                                $scope.getCurrentUser();
                                if (!isDesktop) {
                                    AuthService.saveDeviceToken($scope.user.authToken);
                                }
                            },200);
                            
                        }
                        // ActivityIndicator.hide();
                        $rootScope.hideLoader();
                    }).catch(function(error) {
                        //$cordovaProgress.hide();
                        // ActivityIndicator.hide();
                        $scope.hasError = true;
                        $scope.errMsg = error.message;
                        $scope.login.uNamecls = true;
                        $scope.login.pwdcls = true;
                        console.warn('Error while signingIn :: ', error);
                        // ActivityIndicator.hide();
                        $rootScope.hideLoader();

                    });

                } else {


                    /**
                     * @description
                     * Sets css styles to username and password fields (turns to red when not entered).
                     *
                     * @author
                     * Abrajeethan
                     */

                    if (!$scope.login.userName) {
                        $scope.login.uNamecls = true;
                    }
                    if (!$scope.login.password) {
                        $scope.login.pwdcls = true;
                    }
                }
            };

            /**
             * @description
             * Checks whether the user is registered user or not.
             *
             * @author
             * Tushar
             */

            $scope.emailId = '';

            $scope.forgotPassword = function(emailForm) {
                var email = $scope.emailId;
                console.log("email", email);

                if (emailForm.$invalid) return;

                $rootScope.showLoader();
                AuthService.forgotPassword(email).then(function(response) {
                    console.info('forgotPassword : : ', response);
                    $rootScope.hideLoader();
                    if (response.statusCode == 200){
                        $state.go(AuthRoutes.otp.name);
                    }else if (response.statusCode == 202) {
                        $rootScope.hideLoader();
                        $scope.hasError = true;
                        $scope.errMsg = "Email does not exists";
                    }
                }).catch(function(error) {
                    $rootScope.hideLoader();
                    console.log('Error in forgot password : : ', error);
                    $scope.message.error = error.message;
                    $scope.hasError = true;
                    $scope.errMsg = "Invalid Email";
                });
            };
            $scope.current_pwd = '';

            $scope.changePassword = function(changePwdForm) {

                var checkPass = $scope.current_pwd;
                console.info("check password :::", checkPass);
                var present = {
                        "authToken" : localStorage.getItem('ngStorage-authToken'),
                        "password": checkPass
                    }

                $rootScope.showLoader();
                AuthService.checkPassword(present).then(function(response) {
                    console.info('changePassword : : ', response);
                    $rootScope.hideLoader();
                    if (response.statusCode == 200){
                        $state.go('resetPassword');
                    } else {
                        $scope.changePwdFlag = true;
                        console.log('message', response.message);
                    }
                }).catch(function(error) {
                    $rootScope.hideLoader();
                    console.log('Error in forgot password : : ', error);
                    $scope.changePwdFlag = true;
                });
            };

            /**
             * @description
             * Sends the user provided OTP to server, which checks its validity,
             * and returns response accordingly.
             *
             * @author
             * Tushar
             */


            $scope.validate = function(OTPForm) {

                var otp = $scope.otp.toString();

                $rootScope.showLoader();
                AuthService.validateOTP(otp).then(function(response) {
                    console.info('OTPController[validate] : : ', response);
                    if( response.statusCode == 200){
                        $state.go(AuthRoutes.resetPassword.name);
                    } else if (response.statusCode == 203){
                        $scope.hasError = true;
                        $scope.errMsg = "Invlid code";
                    }else {
                        $scope.hasError = true;
                        $scope.errMsg = response.message;
                    }                    
                    $rootScope.hideLoader();                    
                }).catch(function(error) {
                    console.log('Error while validating OTP : : ', error);
                    $scope.message.error = error.message;
                });
            };

            /**
             * @description
             * Updates/Resets the password with user provided password.
             *
             * @author
             * Tushar
             */

            $scope.reset = function(resetPasswordForm) {
                resetPasswordForm.$submitted = true;
                var password = $scope.actualPwd;

                if (resetPasswordForm.$invalid){
                    return;
                }
                $rootScope.showLoader();
                
                AuthService.resetPassword(password, $rootScope.signUpObj).then(function(response) {
                    console.info('ResetPasswordController[reset] : : ', response);                    
                    $rootScope.hideLoader();
                    if ($rootScope.signUpObj.fromSignUp){  
                        var token = response.userDetails.authToken;
                        var currentUser = response.userDetails;
                        SessionService.create(token, currentUser);

                        if (isDesktop) {
                            localStorage.setItem('currentUser', JSON.stringify(currentUser));
                            $scope.setCurrentUser(currentUser);
                            $state.go('profile.intro');
                        } else {

                            DBService.removeUser(function(){
                                DBService.addCurrentUser(currentUser,function(){
                                    $scope.setCurrentUser(currentUser);
                                    AuthService.saveDeviceToken(token);
                                    $state.go('profile.intro');
                                });
                            });
                        }                     
                    }else{
                        //delete $scope.$localStorage.authToken;
                        $state.go(AuthRoutes.login.name);
                    }                    
                }).catch(function(error) {
                    console.warn('Err   or while reseting password : : ', error);
                    // $scope.message.error = error.message;
                    $scope.errMsg = error.message;
                });
            };

            /* Google Plus login */
            $scope.googleplusLogin = function() {
                $rootScope.showLoader();
                var config = {
                  'client_id': '757048909935-tj9uevcoqec3o6ubk60sn0sm79fp72p9.apps.googleusercontent.com',
                  'scope': 'https://www.googleapis.com/auth/urlshortener'
                };
                gapi.auth.authorize(config, function() {
                  console.log('login complete');
                  console.log("GPlus access_token :: ", gapi.auth.getToken().access_token);
                  var params = {
                        access_token : gapi.auth.getToken().access_token,
                        type    :   "GPlus"
                     } 

                  AuthService.sendAccessToken(params).then(function success(response){
                        console.info('success Response : : ', response);
                        $rootScope.hideLoader();
                        var userDetails = response.userDetails;

                        //SessionService.create(userDetails.token, userDetails);
                        //localStorage.setItem('currentUser', JSON.stringify(userDetails));

                        if (userDetails.isFirstLogin === "yes") {
                            
                            $state.go('profile.intro');
                            $rootScope.hideLoader();
                        } else {
                            //$state.go($scope.profileScreen[screenNumber]);
                            //$state.go('profile.intro');
                            /*which is present in application controller*/
                            //$scope.setCurrentUser(userDetails);
                            setTimeout(function(){
                                $scope.getCurrentUser();
                                if(!isDesktop) {
                                    AuthService.saveDeviceToken($scope.user.authToken);
                                }
                            },200);
                            
                        }
                        // ActivityIndicator.hide();
                        $rootScope.hideLoader();

                     }).catch(function(error) {
                        //$cordovaProgress.hide();
                        // ActivityIndicator.hide();
                        $scope.hasError = true;
                        $scope.errMsg = error.message;
                        $scope.login.uNamecls = true;
                        $scope.login.pwdcls = true;
                        console.warn('Error while signingIn :: ', error);
                        // ActivityIndicator.hide();
                        $rootScope.hideLoader();
                    });

                });
            }

           
             /* Facebook login */
             
            $scope.facebookLogin = function() {
                $rootScope.showLoader();
                FB.login(function(result){
                    console.log("result : ", result);
                    var params = {
                        access_token : result.authResponse.accessToken,
                        type    :   "FB"
                     } 
                     
                     console.log("params : ", params);
                     AuthService.sendAccessToken(params).then(function success(response){
                        console.info('success Response : : ', response);
                        $rootScope.hideLoader();
                        var screenNumber = response.screenNo;
                        var userDetails = response.userDetails;
                        //SessionService.create(userDetails.token, userDetails);
                        localStorage.setItem('currentUser', JSON.stringify(userDetails));
                        if (userDetails.isFirstLogin === "yes") {
                            
                            $state.go('profile.intro');
                        } else {
                            //$state.go($scope.profileScreen[screenNumber]);
                            //$state.go('profile.intro');
                            /*which is present in application controller*/
                            //$scope.setCurrentUser(userDetails);
                            setTimeout(function(){
                                $scope.getCurrentUser();
                                if(!isDesktop) {
                                    AuthService.saveDeviceToken($scope.user.authToken);
                                }
                            },200);
                            
                        }
                        // ActivityIndicator.hide();
                        $rootScope.hideLoader();

                     }).catch(function(error) {
                        //$cordovaProgress.hide();
                        // ActivityIndicator.hide();
                        $scope.hasError = true;
                        $scope.errMsg = error.message;
                        $scope.login.uNamecls = true;
                        $scope.login.pwdcls = true;
                        console.warn('Error while signingIn :: ', error);
                        // ActivityIndicator.hide();
                        $rootScope.hideLoader();
                    });

                }, {scope: 'publish_actions'});
            }
           
            $scope.logout = function() {
                
                $scope.login = {};
                $scope.user = {};
                $scope.currentUser = {};
                localStorage.clear();
                $state.go(AuthRoutes.login.name);
                
            };       
        }]);

})();
