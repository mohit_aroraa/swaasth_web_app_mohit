var diagnostic = angular.module("swaasth.diagnostic", []);

diagnostic.config(['$stateProvider', '$urlRouterProvider', 'diagnosticRoutes', function($stateProvider, $urlRouterProvider, diagnosticRoutes) {
    $stateProvider
            
        .state(diagnosticRoutes.diagnosticView.name, {
            url: diagnosticRoutes.diagnosticView.url,
            templateUrl: 'components/diagnostic/partials/diagnostics.html'
        })

        .state(diagnosticRoutes.testTypeView.name, {
            url: diagnosticRoutes.testTypeView.url,
            templateUrl: 'components/diagnostic/partials/testTypeView.html'
        })

        .state(diagnosticRoutes.diagnosticsList.name, {
            url: diagnosticRoutes.diagnosticsList.url,
            templateUrl: 'components/diagnostic/partials/diagnosticsList.html'
        })

        .state(diagnosticRoutes.diagnosticDetailView.name, {
            url: diagnosticRoutes.diagnosticDetailView.url,
            templateUrl: 'components/diagnostic/partials/diagnosticDetailView.html'
        })

        .state(diagnosticRoutes.addDiagnosticView.name, {
            url: diagnosticRoutes.addDiagnosticView.url,
            templateUrl: 'components/diagnostic/partials/addDiagnostic.html'
        });



}]);