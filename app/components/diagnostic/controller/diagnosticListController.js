(function() {
    'use strict';

    angular.module('swaasth.diagnostic')
        .controller('diagnosticsListController', ['$scope', '$rootScope', 'DiagnosticService', '$stateParams', '$state', function($scope, $rootScope, DiagnosticService, $stateParams, $state ) {

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;
            
            $scope.diagnosticsHeader = $stateParams.params ;
            $scope.medicalDiagnosticDetailsList = [];
            $scope.carousel = {};

            if ($stateParams.index != "-2") {
                $scope.carousel.index = +$stateParams.index || 0;
            }
            $scope.isLoad = true;

            $scope.testName = $stateParams.testName;
            $scope.testId = $stateParams.testId;
            $scope.unitId = $stateParams.unitId;


          /*  $scope.getTestName = function(testName) {
                $scope.tests = DiagnosticService.getStoredTestList();
                for(var i = 0; i <= $scope.tests.length; i++) {
                    if (Number(testName) === $scope.tests[i].id) {
                        return $scope.tests[i].name;
                    }
                }
            };
*/
            $scope.testTypeDisplay = $stateParams.test;

            $scope.getDiagnosticList = function (callback) {
                $rootScope.showLoader();
                $scope.testTypeDisplay = $stateParams.test;

                var category = {
                    authToken: localStorage.getItem('ngStorage-authToken'),
                    param: $scope.diagnosticsHeader,
                    userId: localStorage.getItem('mProfileUserId'),
                    test : $scope.testName
                };
                DiagnosticService.getProfileMedicalInfo(category).then(function(res){
                    console.log('get profile medical info-->', $scope.medicalDiagnosticDetailsList);
                    console.log('get current user',  $scope.currentUser);
                    $rootScope.hideLoader();
                    $scope.isLoad = false;
                    $scope.medicalDiagnosticDetailsList = res.generalInfo[$scope.diagnosticsHeader];

                    if(callback){
                        callback($scope.medicalDiagnosticDetailsList);
                    }
                }).catch(function(error){
                    console.log('error while getting the list view', error);
                });
            };

            $scope.getUnitForId = function(unitId) {
                $scope.units = DiagnosticService.getStoredUnitList();
                for(var i = 0; i <= $scope.units.length; i++) {
                    if (unitId === $scope.units[i].id) {
                        return $scope.units[i].name;
                    }
                }
            };

            $scope.addMedicalInfo = function ($index) {
                console.log('adscreen');
                $state.go('addDiagnosticView', { params: $scope.diagnosticsHeader, testName: $scope.testName, testId: $scope.testId, unitId: $scope.unitId, index: $index });
            };

            $scope.showDiagnosticsDetail = function($index) {
                $state.go('diagnosticDetailView', {  params: $scope.diagnosticsHeader, testName: $scope.testName, testId: $scope.testId, unitId: $scope.unitId, index: $index });
            };

            $scope.generateDetailView = function() {
                
                $scope.getDiagnosticList(function(data){

                    if ($stateParams.index == "-2") {
                        $scope.carousel.index = data.length-1;
                    }
                    $scope.singleDiagnosticDetail = data;
                });

                //$scope.singleDiagnosticDetail = DiagnosticService.getStoredDiagnosticDetails().generalInfo[$scope.diagnosticsHeader];
            };

            $scope.editDiagnosticDetailsInfo = function() {
                console.log('edit screen');
                $state.go('addDiagnosticView', { params: $scope.diagnosticsHeader, testName: $scope.testName, testId: $scope.testId, unitId: $scope.unitId, index: $stateParams.index });
            };

            $scope.deleteMedicalData = function() {
                console.log('carouselIndex',$scope.carousel.index);
                var data = $scope.singleDiagnosticDetail[$scope.carousel.index];
                $rootScope.showLoader();

                var reqObj = {};
                reqObj['authToken'] = localStorage.getItem('ngStorage-authToken');
                reqObj[$scope.diagnosticsHeader] = data;
                reqObj.index = $scope.carousel.index;
                reqObj.userId = localStorage.getItem('mProfileUserId');

                console.log(reqObj);
                DiagnosticService.deleteM(reqObj).then(function(response) {
                    console.info('successfullly saved : : ', response);
                    $rootScope.hideLoader();
                    $scope.goBack();

                }).catch(function(error) {
                    console.warn('error while saving : : ', error);
                });

            };

    }]);

})();