var profile = angular.module("swaasth.profile", []);

profile.config(['$stateProvider', '$urlRouterProvider', 'ProfileRoutes', function($stateProvider, $urlRouterProvider, ProfileRoutes) {
    $stateProvider
        .state(ProfileRoutes.profile.name, {

            url: ProfileRoutes.profile.url,
            // abstract:true,
            // temaplate:'<div ui-view></div>'
            templateUrl: 'components/personal_profile/partials/profile.html',
        })
        .state(ProfileRoutes.introduction.name, {

            url: ProfileRoutes.introduction.url,
            templateUrl: 'components/personal_profile/partials/introduction.html',
            data: { screenNumber: 0 }
        })
        .state(ProfileRoutes.personal.name, {
            url: ProfileRoutes.personal.url,
            templateUrl: 'components/personal_profile/partials/intropersonal.html',
        })
        .state(ProfileRoutes.medical.name, {
            url: ProfileRoutes.medical.url,
            templateUrl: 'components/personal_profile/partials/intromedical.html',
        })
        /*.state(ProfileRoutes.dashboard.name, {
            url: ProfileRoutes.dashboard.url,
            templateUrl: 'components/personal_profile/partials/introdashboard.html',
        })*/
        .state(ProfileRoutes.names.name, {

            url: ProfileRoutes.names.url,
            templateUrl: 'components/personal_profile/partials/profile_names.html',
            data: { screenNumber: 1 }
        })
        .state(ProfileRoutes.gender.name, {

            url: ProfileRoutes.gender.url,
            templateUrl: 'components/personal_profile/partials/profile_gender.html',
            data: { screenNumber: 2 }
        })
        .state(ProfileRoutes.dob.name, {

            url: ProfileRoutes.dob.url,
            templateUrl: 'components/personal_profile/partials/dob.html',
            data: { screenNumber: 3 }
        })
        .state(ProfileRoutes.bloodGroup.name, {

            url: ProfileRoutes.bloodGroup.url,
            templateUrl: 'components/personal_profile/partials/blood_group.html',
            data: { screenNumber: 4 }
        })
        .state(ProfileRoutes.mobile.name, {

            url: ProfileRoutes.mobile.url,
            templateUrl: 'components/personal_profile/partials/phone_number.html',
            data: { screenNumber: 5 }

        })

        .state(ProfileRoutes.address.name, {

            url: ProfileRoutes.address.url,
            templateUrl: 'components/personal_profile/partials/profile_address.html',
            data: { screenNumber: 6 }

        })

        .state(ProfileRoutes.emergencyContact.name, {

            url: ProfileRoutes.emergencyContact.url,
            templateUrl: 'components/personal_profile/partials/emergency_contact.html'

        })
        
        .state(ProfileRoutes.familyMember.name, {

            url: ProfileRoutes.familyMember.url,
            templateUrl: 'components/personal_profile/partials/familyMember.html'

        })

        .state(ProfileRoutes.familyMemberDetail.name, {

            url: ProfileRoutes.familyMemberDetail.url,
            templateUrl: 'components/personal_profile/partials/familyMemberDetails.html'

        });
}]);
