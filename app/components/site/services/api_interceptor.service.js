/*global angular*/
(function() {


    'use strict';


    /**
     * @description 
     * This service intercepts every api request
     * and updates the request/response object.
     * This is useful if, we are passing the same 
     * values to every request object, instead we can
     * add here (at one place) and it will be available
     * to every request. Same applies for response.
     *
     * @author Tushar
     */
    var APIInterceptor = function($localStorage, $injector) {

        /**
         * @description 
         * This is getting called for each request made.
         * Performs the common operation for each request.
         *              
         * @param  {object} request [Request object which made for API call]
         * @return Modified request object. 
         * @author Tushar
         */
        this.request = function(request) {
            var reqCopy = angular.copy(request);
            var authToken = null;
            if (reqCopy.data) {

                /*if (typeof localStorage.getItem('ngStorage-authToken') == 'object') {

                    authToken = JSON.parse(localStorage.getItem('ngStorage-authToken'));//$localStorage.authToken;
                } else {
                    authToken = localStorage.getItem('ngStorage-authToken');
                }*/

                var authString = localStorage.getItem('ngStorage-authToken');

                var splitArray = authString ? authString.split('"'): '';

                if (splitArray && splitArray.length > 1) {
                    authToken = JSON.parse(localStorage.getItem('ngStorage-authToken'));
                } else {
                    authToken = authString;
                }

                reqCopy.data['authToken'] = authToken;

                console.log('AUTH TOKEN -------------------',authToken);
            }
            return reqCopy;
        };

        /**
         * @description 
         * This is getting called if an error is occured 
         * while making an API request.
         * 
         * @param  {object} error [error object which is thrown while making an API call]
         * @return Modified error object.
         * 
         * @author Tushar
         */
        this.requestError = function(error) {

            return error;
        };

        /**
         * @description 
         * This is getting after the successful API call.
         * 
         * @param  {object} response [Response object which we will get after making an API call]
         * @return Modified response object. 
         * 
         * @author Tushar
         */
        this.response = function(response) {
            var $state = $injector.get('$state');
            if(response.data && response.data.statusCode == 204 || response.data.statusCode == 202){
                $state.go('login');
            }

            return response;
        };

        /**
         * @description 
         * This is getting if an error has occured while making an API call.
         * 
         * @param  {object} error [error object which is thrown while making an API call]
         * @return Modified error object. 
         * 
         * @author Tushar
         */
        this.responseError = function(error) {
            var $state = $injector.get('$state');
            if(error.data && error.data.statusCode == 204){
                $state.go('login');
            }

            if(error.status == -1) {
                alert('Unable to connect to the internet. Check your network connection');
            }

            return error;

        };
    };

    APIInterceptor.$inject = ['$localStorage','$injector'];
    angular.module('swaasth')
        .service('APIInterceptor', APIInterceptor);
})();
