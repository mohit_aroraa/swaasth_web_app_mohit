var swaasthApp = angular.module('swaasth');

/**
 * @description
 * Application Level Controller which contains the global application-level data & methods.
 * So other child controllers can make use of this data and methods.
 * 
 * @author
 * Tushar
 */
swaasthApp.controller('ApplicationController', ['$scope', '$rootScope', '$state', 'AuthService', '$localStorage', 'AuthRoutes', 'ProfileService', function($scope, $rootScope, $state, AuthService, $localStorage, AuthRoutes, ProfileService) {
    $scope.message = { error: '', success: '' };
    //$scope.$localStorage = $localStorage;
    $scope.currentUser = {};

    $rootScope.showLogo = true;


    //G L O B A L    R E G E X
    $scope.regexs = {
        onlyAlphabets: /^([A-Za-z. ])+$/,
        onlyNumbers: /\d+/,
        email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        // password: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,20}$/
        password: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,20}$/
    };

    $scope.profileScreen = {
        0: 'profile.intro.personal',
        1: 'names',
        2: 'gender',
        3: 'dob',
        4: 'bloodGroup',
        5: 'mobile',
        6: 'address',
        7: 'profile.intro.medical'
    };

    //var authToken = localStorage.getItem('ngStorage-authToken');

    // Fetching currentUser from localStorage

    //DBService.initDB();

    $scope.getCurrentUser = function () {
        var authToken = localStorage.getItem('ngStorage-authToken');
        //var authToken = $localStorage.authToken;
        if (!$scope.user) {
            //$state.go('login');

            if (isDesktop) {
                var curUser = localStorage.getItem('currentUser');
                var res = JSON.parse(curUser);
                //$scope.currentUser = res
                $scope.user = res;

                console.log('getting current user -->', $scope.currentUser);

                if(res && res.isFirstLogin === "yes") {
                    // $state.go(AuthRoutes.resetPassword.name);
                    $state.go('login');
                    return true;
                }

                if (authToken && res.firstName && res.gender && res.dateOfBirth && res.bloodGroup && res.contactNumber && res.emailId) {
                    $state.go('home');
                } else if (authToken){
                    // localStorage.setItem('screenNo',0);
                    $state.go('profile.intro', {params: $scope.user.id});
                } else {
                    $state.go('login');
                }
            } else {

                DBService.getCurrentUser().then(function(res){
                    $scope.user = res;

                    console.log('getting current user -->', $scope.user);

                    if(res.isFirstLogin === "yes") {
                        // $state.go(AuthRoutes.resetPassword.name);
                        $state.go('login');
                        return true;
                    }

                    if (authToken && res.firstName && res.gender && res.dateOfBirth && res.bloodGroup && res.contactNumber && res.emailId) {
                        $state.go('home');
                    } else if (authToken){
                        // localStorage.setItem('screenNo',0);
                        $state.go('profile.intro', {params:$scope.user.id});
                    } else {
                        $state.go('login');
                    }

                });
            }           
        } else {

            var curUser = localStorage.getItem('currentUser');
            var res = JSON.parse(curUser);

            $scope.user = res;

            if(res.isFirstLogin === "yes") {
                $state.go(AuthRoutes.resetPassword.name);
                return true;
            }

            if (authToken && res.firstName && res.gender && res.dateOfBirth && res.bloodGroup && res.contactNumber && res.emailId) {
                $state.go('home');
            } else if (authToken){
                // localStorage.setItem('screenNo',0);
                $state.go('profile.intro', {params: res.id});
            } else {
                $state.go('login');
            }
        }

    };
    $scope.getCurrentUser();

    $scope.setCurrentUser = function(user) {
        $scope.currentUser = user;
    };

    $scope.isDeviceReady = false;


    $scope.setDeviceReady = function(){
        console.log("set Device ready called");
        $scope.isDeviceReady = true;
    };

    /* Whwn some one click on side bar menu open*/
    $scope.openNav = function() {
        document.getElementById("sideBarSkin").style.width = "250px";
        document.getElementById("sideBarSkin").style.opacity = 1;
    };

    /* Close when someone clicks on the "x" symbol inside the overlay */
    $scope.closeNav = function(event) {
        event.preventDefault();
        event.stopPropagation();
        document.getElementById("sideBarSkin").style.width = "0px";
        document.getElementById("sideBarSkin").style.opacity = 0;
    };


    $scope.goBack = function() {
        var currentState = $rootScope.currentState;
        $rootScope.signUpObj.emailId = "";
        $rootScope.signUpObj.contactNumber = "";
        var unauthenticatedExitArr = ['login'];

        var authenticatedExitArr = ['login','resetPassword', 'profile.intro','home','profile.intro.personal','profile.names','home.recommendationsList','familyMember'];

        if (AuthService.isAuthenticated) {
            // var lastUnsavedScreenNumber = ProfileService.getlastUnsavedScreenNumber();
            // var stateScreenNumber = $rootScope.$state.$current.data.screenNumber;
            // console.info('stateScreenNumber : :', stateScreenNumber);
            // console.info('lastUnsavedScreenNumber : :', lastUnsavedScreenNumber);

            /*if (authenticatedExitArr.indexOf(currentState) !== -1 || lastUnsavedScreenNumber === stateScreenNumber) {
                $rootScope.$broadcast('AUTHENTICATED:EXIT');
                navigator.app.exitApp();
            }*/

            if (currentState == 'profile.names' && $scope.currentUser && $scope.currentUser.firstName && $scope.currentUser.gender && $scope.currentUser.dateOfBirth && $scope.currentUser.bloodGroup && $scope.currentUser.contactNumber && $scope.currentUser.emailId) {
                $state.go('familyMember');
                return true;
            } else {

                if (authenticatedExitArr.indexOf(currentState) !== -1) {
                    $rootScope.$broadcast('AUTHENTICATED:EXIT');
                    
                    var confirmText = confirm('Are you sure you want to exit.');

                    console.log('confirmText ==>', confirmText);

                    if (confirmText == true) {   
                        navigator.app.exitApp();
                    } else {
                        return true;
                    }

                    
                }
            }
            // if (currentState.match('profile')) {
            //     return;
            // }

        } else {


                if (unauthenticatedExitArr.indexOf(currentState) !== -1) {
                    var confirmText = confirm('Are you sure you want to exit.')

                    if (confirmText == true) {
                        console.log('type ==>', type)
                        navigator.app.exitApp();
                    } else {
                        return true;
                    }
                } /*else {

                    if (currentState == 'resetPassword') {
                        return $state.go('login');
                    }
                }*/
            //}
        }
        navigator.app.backHistory();        

        if($rootScope.previousState == "allergieAddDescription" && $rootScope.currentState == "alleryDetailedView" ) {
            navigator.app.backHistory();
            navigator.app.backHistory();
            navigator.app.backHistory();
            navigator.app.backHistory();
        }

        if($rootScope.previousState == "healthInsurance" && $rootScope.currentState == "healthInsuranceDetailView" ) {
            navigator.app.backHistory();
        }


        if($rootScope.previousState == "addDiagnosticView" && $rootScope.currentState == "diagnosticDetailView" ) {
            navigator.app.backHistory();
        }
    };

    $scope.addMedicalView = function () {
        console.log('add screen');
        $scope.addScreen();
    };

    $scope.setPasswordChange = function () {
            $scope.isSocialUser = $scope.user.isSocialUser || false;
            if ($scope.isSocialUser){
                $scope.set_or_change_pass_url = "resetPassword";
                $scope.set_or_change_pass_text = "Set Password";
            }else{
                $scope.set_or_change_pass_url = "changePassword";
                $scope.set_or_change_pass_text = "Change Password";
            }
    }


    $scope.checkGender = function() {
        if ($scope.currentUser && $scope.currentUser.gender) {
            if ($scope.currentUser.gender == 'M') {
                $scope.isMale = true;
            } else if ($scope.currentUser.gender == 'F') {
                $scope.isFemale = true;
            } else {
                $scope.isOthers = true;
            }
        }
    }

    $scope.goToUnfilledScreen = function() {

        var screenNo = localStorage.getItem('screenNo');

        if (+screenNo) {
            $state.go($scope.profileScreen[screenNo], { params: $scope.currentUser.id});
        } else {
            localStorage.setItem('screenNo', 1);
            $state.go($scope.profileScreen[0], { params: $scope.currentUser.id });
        }

        // $scope.screenNum = screenNo;

    };

    /**
     * @description
     * Saves the user details to database and moves to next screen
     *
     * @author
     * Tushar
     */
    $scope.nextAction = function(type) {

        $scope.screenNum = 7;

        var currentScreenNumber = $rootScope.$state.$current.data.screenNumber;
        var nextScreen = currentScreenNumber + 1;

        //this is for newly adding user
        if (type == 'new') {
            $state.go($scope.profileScreen[nextScreen], { params: 'new' });
            $scope.screenNum = nextScreen;
            return true;
        }

        //this is for editing the user details
        if (type == 'edit') {
            $scope.nextSave(type);
            return true;
        }

        //this is for first time login user
        //$rootScope.showLoader();    
        //if (!param) {
        $scope.screenNum = nextScreen;

        if (!isDesktop) {
            DBService.updateCurrentUser($scope.user);
        }
        localStorage.setItem('screenNo', nextScreen);
        $state.go($scope.profileScreen[nextScreen], { params: type });
        //} 
        /*else {
            $state.go($scope.profileScreen[nextScreen], { params: 'new' });

        }*/

        //$scope.nextSave();
        //$rootScope.hideLoader();
    };

    $scope.nextSave = function(type) {
        // ActivityIndicator.show();
        $rootScope.showLoader();
        // $scope.user.country = 96;
        ProfileService.save($scope.currentUser).then(function(response) {
            console.log('profileController[next] : : ', response);
            // ProfileService.setProfileProgress($scope.progress.profile);
            // ActivityIndicator.hide();
            $rootScope.hideLoader();
            if (response.statusCode == 200) {
                // $state.go('profile.intro.medical');
                localStorage.setItem('screenNo', 7);

                if (type != 'new' && type != 'edit') {
                    //this for first time
                    $scope.goToUnfilledScreen();
                } else if (type != 'edit') {
                    //this is for adding family member
                    //after adding all the details
                    $state.go('familyMember');
                } else {
                    //this is for after editing
                    $scope.showPersonalDetail();
                }

                /*if ($scope.currentUser.isPrimary == 'Yes') {
                    DBService.updateCurrentUser($scope.user);
                }*/
                $scope.setCurrentUser();
            }

        }).catch(function(error) {
            console.log('profileController[error] : : ', error);
            /*if(error.statusCode == 202){
                $state.go('login');
                // DBService.removeUser();
            }*/
            // ActivityIndicator.hide();
            $rootScope.hideLoader();
        });
    };

    $scope.isClicked = function(gender) {

        switch (gender) {
            case 1:
                $scope.isFemale = true;
                $scope.isMale = $scope.isOthers = false;
                $scope.currentUser.gender = "F";
                $scope.genderTxt = "Female";
                break;
            case 2:
                $scope.isMale = true;
                $scope.isFemale = $scope.isOthers = false;
                $scope.currentUser.gender = "M";
                $scope.genderTxt = "Male";
                break;
            case 3:
                $scope.isOthers = true;
                $scope.isMale = $scope.isFemale = false;
                $scope.currentUser.gender = "O";
                $scope.genderTxt = "OTHERS";
                break;
        }

    };

    $scope.showPersonalDetail = function() {
        //profile.intro.personal
        //$state.go('profile.intro.personal');
        $state.go('familyMemberDetail', { params: $scope.currentUser.id });

    };

    $scope.logout = function() {

        $scope.user = {};
        $scope.currentUser = {};
        localStorage.clear();
        $state.go(AuthRoutes.login.name);
        
    }; 

}]);
