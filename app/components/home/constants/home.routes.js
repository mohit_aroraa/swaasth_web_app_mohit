/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.home').constant('HomeRoutes', {

        /**********************************************************************
                    PROFILE ROUTES
        **********************************************************************/
       home: {
            name: 'home',
            url: '/home'
        },

        recommendations: {
            name: 'home.recommendationsList',
            url: '/recommendationsList'
        },

        recommendationDetail: {
            name: 'recommendationDetail',
            url: '/recommendationDetail/:params'
        },

        aboutSwasth: {
            name: 'aboutSwasth',
            url: '/aboutSwasth'
        },

        termSwasth: {
            name: 'termSwasth',
            url: '/termSwasth'
        },

        privacyPolicy: {
            name: 'privacyPolicy',
            url: '/privacyPolicy'
        },

        contactUs: {
            name: 'contactUs',
            url: '/contactUs'
        }

    });
})();
