/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.snapshots').constant('SnapShotsRoutes', {

        /**********************************************************************
                    PROFILE ROUTES
        **********************************************************************/

        snapShotView: {
            name: 'snapShotView',
            url: '/snapShotView/:params'
        }

    });
})();