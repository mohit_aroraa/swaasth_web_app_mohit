var snapshotsModule = angular.module("swaasth.snapshots", []);

snapshotsModule.config(['$stateProvider', '$urlRouterProvider', 'SnapShotsRoutes', function($stateProvider, $urlRouterProvider, SnapShotsRoutes) {
    $stateProvider
    
        .state(SnapShotsRoutes.snapShotView.name,{

            url: SnapShotsRoutes.snapShotView.url,
            templateUrl: 'components/snapshot/partials/snapshots.html'

        })

}]);