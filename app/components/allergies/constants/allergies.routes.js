/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.allergies').constant('AllergiesRoutes', {

        /**********************************************************************
                    PROFILE ROUTES
        **********************************************************************/

        userAllergyView: {
            name: 'userAllergyView',
            url: '/userAllergyView/:params'
        },

        alleryDetailedView: {
            name: 'alleryDetailedView',
            url: '/alleryDetailedView/:params?index'
        },
        
        allergies: {
            name: 'allergies',
            url: '/allergies'
        },

        allergiesTriggers: {
            name: 'allergiesTriggers',
            url: '/allergiesTriggers/:allergyId'
        },
        
        allergiesSymptoms: {
            name: 'allergiesSymptoms',
            url: '/allergiesSymptoms/:allergyId/:allergyName/:triggerBy'
        },

        allergieAddDescription: {
            name: 'allergieAddDescription',
            url: '/allergieAddDescription/:allergyId/:allergyName/:triggerBy/:symtomsList?index'
        }

    });
})();
