/**
 * @description
 * 
 *
 * @author
 * Koushik
 */
(function() {

    'use strict';

    angular.module('swaasth.allergies')
        .controller('userAllergyListController', ['$scope', '$state', '$rootScope', '$stateParams', 'AllergyService', function($scope, $state, $rootScope, $stateParams, AllergyService) {

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;
            
        	$scope.carousel = {};
            $scope.carousel.index = +$stateParams.index || 0;

        	$scope.medicalUserAllergiesList = [];
        	$scope.isLoad = true;
        	var param = $stateParams.params;
            $scope.allery = [];
            $scope.singleAllery = [];
 

    /*Show The List of Allergies for User */
        	$scope.getAllergiesListForUser = function (callback) {
                $rootScope.showLoader();
                $scope.testTypeDisplay = $stateParams.test;
                console.log("Getting Allery List ::");
                var usedDetails = {
                    authToken: localStorage.getItem('ngStorage-authToken'),
                    userId: localStorage.getItem('mProfileUserId')
                };
                AllergyService.fetchAllergyDetails(usedDetails).then(function(res){
                    console.log("userdDetails");
                    $rootScope.hideLoader();
                    $scope.isLoad = false;
                    $scope.medicalUserAllergiesList = res.result.allergies;

                    if(callback){
                        callback($scope.medicalUserAllergiesList);
                    }
                }).catch(function(error){
                    console.log('error while getting the list view', error);
                });
            };

            $scope.showUserAllergyDetail = function($index) {
            	$state.go('alleryDetailedView', { params:  param, index: $index});
            };

            $scope.generatealleryDetilsView = function() {
                $scope.getAllergiesListForUser(function(data){
                    console.log("success Got List ::-->", data);
                    $scope.singleAllery = data;
                });
                //$scope.singleAllery = AllergyService.getAllergiesForUser();
            };

            $scope.addMedicalInfo = function() {
            	console.log('adscreen');
                $state.go('allergies');
            };

            $scope.editAllergyInfo = function(carouselIndex) {
                var data = $scope.singleAllery[$scope.carousel.index];
                $state.go('allergieAddDescription', { allergyId:data.id, allergyName: data.allergy,  triggerBy: data.triggeredBy, symtomsList: data.symptoms, index: carouselIndex});
            };

            $scope.deleteAllergyData = function() {
                console.log('carouselIndex',$scope.carousel.index);
                var data = $scope.singleAllery[$scope.carousel.index];
                $rootScope.showLoader();

                var reqObj = {
                    authToken: localStorage.getItem('ngStorage-authToken'),
                    userId: localStorage.getItem('mProfileUserId'),
                    type: "delete",
                    id: data.id
                };

                console.log(reqObj);
                AllergyService.CRUDAllergyDetails(reqObj).then(function(response) {
                    console.info('successfullly Deleted : : ', response);
                    $rootScope.hideLoader();
                    $scope.goBack();

                }).catch(function(error) {
                    console.warn('error while saving : : ', error);
                });
            };

            $scope.goToListPage = function() {
                var allergy = "allergies";
                $state.go('userAllergyView', { params:  allergy});
            };

    }]);
})();
