/**
 * @description
 * 
 *
 * @author
 * Koushik
 */
(function() {

    'use strict';

    angular.module('swaasth.allergies')
        .controller('allergiesTriggersController', ['$scope', '$state', 'AllergyService', '$stateParams', '$rootScope', function($scope, $state, AllergyService, $stateParams, $rootScope) {

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;
            
        	$scope.allegyList = AllergyService.getStoredAllergiesList();

        	$scope.getAllergyName = function(id) {
        		for (var i = 0; i <= $scope.allegyList.length; i++) {
	        		if($scope.allegyList[i].id == Number(id)){
	        			return $scope.allegyList[i].name;
	        		}
        		}
        	};

        	$scope.allergyName = $scope.getAllergyName($stateParams.allergyId);

        	$scope.triggeredList = [];

    /* Get trigger list using allergy Id*/
        	$scope.getTriggerList = function() {
                $rootScope.showLoader();
                var allergiesID = {
                    "allergyId": $stateParams.allergyId
                }

                AllergyService.getTriggersList(allergiesID).then(function(res) {
                    $rootScope.hideLoader();
        			$scope.triggeredList = res.triggers;
        		}).catch(function(error) {
        			console.info("Failed to load Triggered List", error);
        		});

        	};


        	$scope.goToallergiesDetails = function(allergyName, triggerBy) {
        		$state.go('allergiesSymptoms',{ allergyId: $stateParams.allergyId, allergyName: allergyName, triggerBy: triggerBy });
        	};
    }]);
})();
