/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.mprofile').constant('MedicalRoutes', {

        /**********************************************************************
                    PROFILE ROUTES
        **********************************************************************/
        medicalinfo: {
            name: 'medicalinfo',
            url: '/medicalinfo/:params'
        },

        medicalListView: {
            name: 'medicalListView',
            url: '/medicalListView/:params'
        },

        medicalProfileAdd: {
            name: 'medicalProfileAdd',
            url: '/medicalProfileAdd/:params/:index'
        },

        medicalNoteView: {
            name: 'medicalNoteView',
            url: '/medicalNoteView'
        },

        medicalDetailView: {
            name: 'medicalDetailView',
            url: '/medicalDetailView/:params/:index'
        }
        
    });
})();
