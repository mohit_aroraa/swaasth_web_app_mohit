var mprofile = angular.module("swaasth.mprofile", []);

mprofile.config(['$stateProvider', '$urlRouterProvider', 'MedicalRoutes', function($stateProvider, $urlRouterProvider, MedicalRoutes) {
    $stateProvider
        .state(MedicalRoutes.medicalinfo.name,{

            url: MedicalRoutes.medicalinfo.url,
            templateUrl: 'components/medical_profile/partials/medicalProfileView.html'

        })

        .state(MedicalRoutes.medicalListView.name, {
            url: MedicalRoutes.medicalListView.url,
            templateUrl: 'components/medical_profile/partials/medicalListView.html'
        })

        .state(MedicalRoutes.medicalProfileAdd.name, {
            url: MedicalRoutes.medicalProfileAdd.url,
            templateUrl: 'components/medical_profile/partials/medicalProfileAdd.html'
        })

        .state(MedicalRoutes.medicalNoteView.name, {
            url: MedicalRoutes.medicalNoteView.url,
            templateUrl: 'components/medical_profile/partials/medicalNoteView.html'
        })

        .state(MedicalRoutes.medicalDetailView.name, {
            url: MedicalRoutes.medicalDetailView.url,
            templateUrl: 'components/medical_profile/partials/medicalDetailView.html'
        })
}]);