/**
 * Created by abrajeethan on 7/4/16.
 */
/*global angular */
/*(function() {
    'use strict';
*/

    /**
     * @description This is useful for comparing the two fields.
     *              In-short, It checks whether the two fields are same or not.
     *
     * @author Tushar
     */

    angular.module('swaasth.mprofile').directive('convertTo', function() {
        return {
            scope: {
                targetModel: '=convertTo',
                gender:'='
            },
            require: 'ngModel',
            link: function postLink(scope, element, attrs, ngModel) {

                var gender = scope.gender;
                var convert = function() {
                    //console.log(scope, element, attrs, ngModel);
                    var sourceValue = element.val();
                    console.log("inside Height convert directive=",element.val(),attrs.ngModel);
                    var targetValue = scope.targetModel;
                    var retValue = 0;
                    if(attrs.ngModel=="heightCM"){
                        var initVal = (sourceValue/30.48);
                        var feet = Math.floor(initVal);
                        var inch = Math.round((initVal%1)*12);
                        retValue = Number(feet+"."+inch);
                        console.log("retValue",retValue);
                        chooseClass(retValue,gender);
                    } else {
                        retValue = Math.round((sourceValue)*30.48);
                        chooseClass(retValue,gender);
                    }


                    if (targetValue !== null) {
                        //return sourceValue === targetValue;
                    }

                    return retValue;
                };

                var chooseClass

                scope.$watch(convert, function(newValue) {
                    scope.targetModel = newValue;
                    //ngModel.$setValidity('errorCompareTo', newValue);
                });
            }
        };
    }).directive('addMcode', function() {
        return {
            require: 'ngModel',
            link: function postLink(scope, element, attrs, ngModel) {
                element.bind('focus',function(){
                    scope.$apply(function(){
                        if(!ngModel.$viewValue || ngModel.$viewValue == '' || (ngModel.$viewValue != '+91 ' && ngModel.$viewValue.length <= 4)){
                            ngModel.$setViewValue('+91 ');
                            ngModel.$render(); 
                        }
                    });
                });

                element.bind('blur',function(){
                    scope.$apply(function(){
                        if(ngModel.$viewValue == '+91 '){
                            ngModel.$setViewValue('');
                            ngModel.$render(); 
                        }
                    });
                });

                element.bind('input',function(){
                    scope.$apply(function(){
                        if(ngModel.$viewValue.length < 4 && ngModel.$viewValue.match(/\+91 /)){
                            ngModel.$setViewValue('+91 ');
                            ngModel.$render(); 
                        } else if(ngModel.$viewValue.length >= 10 && !ngModel.$viewValue.match(/\+91 /)){
                            ngModel.$setViewValue(ngModel.$viewValue.slice(0,10));
                            ngModel.$render(); 
                        }
                    });
                });
            }
        }
    });
/*})();
*/