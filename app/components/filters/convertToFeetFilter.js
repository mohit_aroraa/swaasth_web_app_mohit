angular.module('swaasth').filter('convertToFeet', [function () {
    return function(input) {
    	var initVal = (input/30.48);
		var feet = Math.floor(initVal);
		var inch = Math.round((initVal%1)*12);
		retValue = feet+'\' '+  inch+'\"';
      return retValue;
    };
  }]);